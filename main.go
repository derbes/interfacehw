package main

import(
	"fmt"
	"math"
)

type Shape interface {
	Area()
}
type Rectangle struct{
	a int
	b int
}
type Circle struct{
	radius float32
}
type Triangle struct{
	a float64
	b float64
	c float64
}
type Square struct{
	a int
}

func (circle * Circle) Area(){
	fmt.Println(math.Pi * circle.radius * circle.radius)
}

func (r * Rectangle) Area(){
	fmt.Println(r.a * r.b)
}

func (t * Triangle) Area(){
	hp:=  (t.a + t.b + t.c)/2
	heron:= (hp * (hp-t.a) * (hp-t.b) * (hp-t.c))

	fmt.Println(math.Sqrt(heron))
}
func (s * Square) Area(){
	fmt.Println(s.a * s.a)
}

func main(){

	var rectangle Shape = &Rectangle{3,4}
	rectangle.Area()

	var circle Shape = &Circle{3}
	circle.Area()

	var triangle Shape = &Triangle{11, 11, 7.5}
	triangle.Area()

	var square Shape = &Square{4}
	square.Area()
}